import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import App from './routers/App';

import './index.css';

const divRoot = document.querySelector("#root");

const saludo = "Hola";
ReactDOM.render(
<BrowserRouter> 
    <App/>
</BrowserRouter>, divRoot )