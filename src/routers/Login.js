import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import SignIn from './SignIn';

const Login = () => {

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
                                            
    const handleLogin = (e) => {
        e.preventDefault();
        console.log(username, password);
        const headers = new Headers();
        headers.set('Authorization', 'Basic ' + Buffer.from(username + ":" + password).toString('base64'));

        //Validate username and password

        const loginRequest = fetch(`http://localhost:3001/login`, { 
                                    method: 'POST',
                                    headers: headers })

        loginRequest.then( resp => resp.json())
            .then( console.log )
            .catch (console.warn)
      
    }
    
    return (
        <>
        <h1> LOGIN</h1>
        <div id="Contenedor">
        <form onSubmit={ handleLogin }>
            <label>
                <input 
                    type="text"
                    value={username} 
                    placeholder="Username"
                    onChange={ e => setUsername(e.target.value)}/>
            </label>
            <label>
                <input
                    type="text"
                    value= {password}
                    placeholder="Password"
                    onChange={ e => setPassword(e.target.value)}/>
            </label>
            <br></br>
            <input type="submit" value="Login" />
        </form>
        <Router>
      <div>
        <ul>
          <li>
            <Link to="/SignIn">SignIn</Link>
          </li>
        </ul>

        <Switch>
          <Route path="/SignIn">
            <SignIn />
          </Route>
        </Switch>
      </div>
    </Router>
        </div>
        </>
    )
}

Login.propTypes = {

    username: PropTypes.string.isRequired,
    password: PropTypes.string.isRequired,
}

Login.defaultProps = {
    username: '',
    password: '',
}

export default Login
