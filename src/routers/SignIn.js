import {useState} from 'react'
import PropTypes from 'prop-types'

const SignIn = () => {

    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [firstname, setFirstname] = useState('');
    const [lastname, setLastname] = useState('')
    const [password, setPassword] = useState('');

    const handleSignIn = ( e ) => {

        e.preventDefault();
        console.log(username, password);

        //Validate username and password...

        const user = {
            username,
            email,
            firstname,
            lastname,
            password
            };

            const headers = new Headers();
            headers.set('Accept', 'application/json' );
            headers.set('Content-Type', 'application/json');
    
        const signInRequest = fetch(`http://localhost:3001/users`, { 
                                    method: 'POST',
                                    headers: headers,
                                    body: JSON.stringify(user) })

        signInRequest.then( resp => resp.json())
            .then( console.log )
            .catch (console.warn)
      
    
    }


    return (
        <>
        <h1> SIGN IN</h1>
        <div id="Contenedor">
        <form onSubmit={ handleSignIn }>
            <label>
                <input 
                    type="text"
                    value={username} 
                    placeholder="Username *"
                    onChange={ e => setUsername(e.target.value)}/>
            </label>
            <label>
                <input 
                    type="text"
                    value={email} 
                    placeholder="Email *"
                    onChange={ e => setEmail(e.target.value)}/>
            </label>
            <label>
                <input 
                    type="text"
                    value={firstname} 
                    placeholder="Firstname"
                    onChange={ e => setFirstname(e.target.value)}/>
            </label>
            <label>
                <input 
                    type="text"
                    value={lastname} 
                    placeholder="Lastname"
                    onChange={ e => setLastname(e.target.value)}/>
            </label>
            <label>
                <input
                    type="text"
                    value= {password}
                    placeholder="Password *"
                    onChange={ e => setPassword(e.target.value)}/>
            </label>
            <br></br>
            <input type="submit" value="Sign In" />
        </form>
        </div>
        </>
    )
}

SignIn.propTypes = {
    username: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    firstname: PropTypes.string,
    lastname: PropTypes.string,
    password: PropTypes.string.isRequired,

}

SignIn.defaultProps = {
    username: '',
    email: '',
    password: '',
}

export default SignIn
