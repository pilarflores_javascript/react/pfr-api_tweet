import React from 'react';
import PropTypes from 'prop-types';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import Login from './Login';
import SignIn from './SignIn';
import Headers from '../components/Headers';
import Notes from '../components/Notes';
import Calendary from '../components/Calendary';


const App = () => {
    return (
        <>
        <Headers></Headers>
       {/* <h1>Tweek with React and NodeJS</h1>
         <Router>
          <ul >
      
            <li >
            <Link to="/login">Login</Link></li>
            
            <li >
            <Link to="/signin">Sign In</Link></li>

          </ul>
          <Switch>
            <Route exact path="/" />
            <Route exact path="/login" component={Login}/>
            <Route exact path="/signin" component={SignIn}/>
          </Switch>
        </Router> */}
        <Calendary></Calendary>
        <Notes></Notes>

      </>
    )
}

App.propTypes = {

}

export default App
