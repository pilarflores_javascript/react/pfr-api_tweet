import React from 'react'
import PropTypes from 'prop-types'
import '../css/Headers.css'
import '../resource/images/user.png'

const date = new Date();
const options = {  year: 'numeric', month: 'long' };

const Headers = () => {
    return (
        <div className="Headers">
            <div className="Headers-Date"> {date.toLocaleDateString("en-US", options)} </div>
            <div className="Headers-Toolbar"> 
                <button className="Headers-Buttom" onClick={()=> console.log("abrir modal")}>PF</button>
                <button className="Headers-Buttom" onClick={() =>console.log("Mostrar cuadro tools ")}>{"..."}</button>
                <button className="Headers-Buttom" onClick={() =>console.log("Mostarr tareas semana anterior")}>{"<"}</button>
                <button className="Headers-Buttom" onClick={() =>console.log("Mostarr tareas semana siguiente")}>{">"}</button>
            </div>
            
        </div>
    )
}

Headers.propTypes = {

}

export default Headers
 